/*
* У папці calculator дана верстка макета калькулятора. 
Потрібно зробити цей калькулятор робочим.
* При натисканні на клавіші з цифрами - набір введених цифр має бути показаний на табло калькулятора.
* При натисканні на знаки операторів (`*`, `/`, `+`, `-`) на табло нічого не відбувається - програма чекає введення другого числа для виконання операції.
* Якщо користувач ввів одне число, вибрав оператор і ввів друге число, то при натисканні як кнопки `=`, так і будь-якого з операторів, в табло повинен з'явитися результат виконання попереднього виразу.
* При натисканні клавіш `M+` або `M-` у лівій частині табло необхідно показати маленьку букву `m` - це означає, що в пам'яті зберігається число. Натискання на MRC покаже число з пам'яті на екрані. Повторне натискання `MRC` має очищати пам'ять.
*/

/*
1. Зєжнати верстку з джс 
Потрібно зробити цей калькулятор робочим.
2. Знайти всі кнопки та спрбувати їх вивести у консоль
3. Записти перші числа в память коду 
4. вивести числа на екран
5. додати знаки ар. операцій 
6. Знайти другі числа 
7. Вивести другі числа 
8. Вивести результат операції
*/

const calculate = {
    operand1 : "",
    sign : "",
    operand2 : "",
    rez : "",
    mem: "",
    displey: ""
}

let flag = false;
const validate = (r, v) => r.test(v);

document.querySelector(".keys").addEventListener("click", (e) => {
   
   // нажато цифри перший раз
    if(validate(/[\d.]/, e.target.value)){
        if(calculate.operand2 ==="" && calculate.sign ==="" ){
        calculate.operand1 += e.target.value;
        calculate.displey=calculate.operand1
        show(calculate.operand1);
        
        console.log(`operand1=${calculate.operand1}  sign=${calculate.sign} operand2=${calculate.operand2} `)
        }
        else  if(calculate.sign !==""){
            calculate.operand2 += e.target.value;
        calculate.displey=calculate.operand2

            show(calculate.operand2);
           
        console.log(`operand1=${calculate.operand1}  sign=${calculate.sign} operand2=${calculate.operand2} `)



            
        }

    }
    //нажатий символ (- + * /)
     if(calculate.sign ===""&& validate(/([-+/*])$/, e.target.value)){
        calculate.sign = e.target.value
       
        console.log(`operand1=${calculate.operand1}  sign=${calculate.sign} operand2=${calculate.operand2} `)


    }

    

    //нажато знак =
     if (validate(/\=/, e.target.value)){
        switch (calculate.sign) {
            case "*":
               calculate.rez = calculate.operand1 * calculate.operand2;
               break
             
            case "+":
              calculate.rez = +calculate.operand1 + +calculate.operand2;
              break
            case "-":
              calculate.rez = calculate.operand1 - calculate.operand2;
              break
            case "/":
              if (calculate.operand2 != 0) {
                 calculate.rez = calculate.operand1 / calculate.operand2;
                 break
              } else {
               calculate.rez = "Помилка"
                break
                
              }
              
          }
        show(calculate.rez);
                console.log("rez="+calculate.rez )



        
        if(calculate.rez === "Помилка"){
            calculate.operand1 = "";
        calculate.displey=calculate.operand1

        }else{
            calculate.operand1 = calculate.rez;
        calculate.displey=calculate.operand1

        }
        calculate.sign = "";
        calculate.operand2 = "";
        calculate.rez = "";



    }
    //нажато символ с
    if(validate(/C/, e.target.value)){
        calculate.sign = "";
        calculate.operand2 = "";
        calculate.rez = "";
        calculate.operand1 = "";
        calculate.displey=calculate.operand1

        show(calculate.operand1);
        console.log(`operand1=${calculate.operand1}  sign=${calculate.sign} operand2=${calculate.operand2} `)


    }
    //нажато символ (- + * /) - повторно (замість знаку =)

    if(calculate.operand1 !=="" &&  calculate.operand2 !==""){
        
        if(validate(/[-+/*]/, e.target.value)){
        switch (calculate.sign) {
            case "*":
               calculate.rez = calculate.operand1 * calculate.operand2;
               break
             
            case "+":
              calculate.rez = +calculate.operand1 + +calculate.operand2;
              break
            case "-":
              calculate.rez = calculate.operand1 - calculate.operand2;
              break
            case "/":
              if (calculate.operand2 != 0) {
                 calculate.rez = calculate.operand1 / calculate.operand2;
                 break
              } else {
               calculate.rez = "Помилка"
                break
                
              }
              
          }
        show(calculate.rez);
        console.log("rez="+calculate.rez )


        calculate.operand1 = calculate.rez;
        if(calculate.rez === "Помилка"){
            calculate.operand1 = "";
        calculate.displey=calculate.operand1

        }else{
            calculate.operand1 = calculate.rez;
        }
        calculate.sign = e.target.value;
        calculate.operand2 = "";
        calculate.rez = "";
        calculate.displey=calculate.operand1

      }
      

    }
    //нажато символ m+/m- 

    if(validate(/m\+ /, e.target.value)){
      calculate.mem=Number(calculate.displey)+Number(calculate.mem)
      console.log("m+"+" "+"="+" "+calculate.mem)
      m();
      }
    if(validate(/m\- /, e.target.value)){
      calculate.mem= Number(calculate.mem)-Number(calculate.displey)
      console.log("m-"+" "+"="+" "+calculate.mem)
      m();

      }

    //нажато символ mrc 

    if(validate(/mrc/, e.target.value)){
      if(flag!==true){
      flag = true
      show(calculate.mem);
      console.log("mem"+" "+ "="+ calculate.mem)
      }else{
         flag = false
        calculate.operand1=calculate.mem=""
        mNone();
        show(calculate.operand1);
        console.log("mem"+" "+ "="+ "")


      }
      
      }
      if(calculate.operand1 !=="" &&  calculate.sign !==""){
        if(validate(/mrc/, e.target.value)){
          calculate.operand2=calculate.mem

        }
      }
      if(calculate.operand1 ==="" &&  calculate.sign ===""){
        if(validate(/mrc/, e.target.value)){
          calculate.operand1=calculate.mem

        }
      }
      //активуємо кнопку =
    if(calculate.operand1 !=="" &&  calculate.operand2 !=="" &&  calculate.sign !==""){
      document.querySelector(".orange").disabled=false
    }else if(calculate.operand2 ===""  ){
      document.querySelector(".orange").disabled=true

    }
      
})

function show (v) {
    const d = document.querySelector(".display input");

    d.value = v;
}
function m(){
  const mSpan=document.querySelector(".display > span")
      mSpan.textContent="m"
      mSpan.style.display="block"

}
function mNone(){
  const mSpan=document.querySelector(".display > span")
      mSpan.style.display="none"
     
}



